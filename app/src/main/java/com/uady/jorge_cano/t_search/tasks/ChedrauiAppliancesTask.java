package com.uady.jorge_cano.t_search.tasks;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.uady.jorge_cano.t_search.activities.ResultsActivity;
import com.uady.jorge_cano.t_search.model.Appliance;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


/**
 * Created by jorge-cano on 2/10/17.
 */

public class ChedrauiAppliancesTask extends AsyncTask<Void, Void, ArrayList<Appliance>> {

    private String searchUrl = "http://www.chedraui.com.mx/index.php/meridanorte/endeca/result/?&N=4294967263+&Ntt=";
    private String searchTerm;
    private String domain = "http://www.chedraui.com.mx";
    private ResultsActivity activity;

    public ChedrauiAppliancesTask(String searchTerm, ResultsActivity activity) {
        this.searchTerm = searchTerm;
        this.activity = activity;
    }

    @Override
    protected ArrayList<Appliance> doInBackground(Void... params) {

        Document document = null;
        try {
            document = Jsoup.connect(this.searchUrl + this.searchTerm)
                    .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0")
                    .referrer(this.searchUrl + this.searchTerm)
                    .followRedirects(true)
                    .timeout(5000)
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Language", "en")
                    .header("Accept-Encoding", "gzip,deflate,sdch")
                    .header("Host", "www.chedraui.com.mx")
                    .execute().parse();
            Elements results = document.select("div.item");

            ArrayList<Appliance> appliances = parseAppliances(results);

            return appliances;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return new ArrayList<>();
    }

    @Override
    protected void onPostExecute(ArrayList<Appliance> appliances) {
        this.activity.setApplianceSearchResults(appliances);
    }

    private ArrayList<Appliance> parseAppliances(Elements results) throws IOException{

        ArrayList<Appliance> appliances = new ArrayList<>();

        try {
            for (Element result : results) {
                String productDetailsUrl = result.select("a").first().attr("href");
                Document document = Jsoup.connect(productDetailsUrl)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0")
                        //.referrer("http://www.google.com")
                        .followRedirects(true)
                        .timeout(1200)
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("Accept-Language", "en")
                        .header("Accept-Encoding", "gzip,deflate,sdch")
                        .header("Host", "www.chedraui.com.mx")
                        .get();

                String model = document.select("div.product-name").select("span.h1").first().text();

                Bitmap image = getImageBitmap(document.select("a.ma-a-lighbox").attr("href"));

                String stringPrice = document.select("span.price").text();
                BigDecimal price = new BigDecimal(stringPrice.substring(1));
                String priceCode = "MXN";

                String brand = "N/A";

                int intake = 0;

                String assessment = "N/A";


                String dimensions = "N/A";

                String capacity = "N/A";

                String store = "Chedraui";

                appliances.add(new Appliance(model, image, price, priceCode,
                        intake, assessment, dimensions, capacity, store, brand, productDetailsUrl));

            }
        } catch (SocketTimeoutException e){
            return appliances;
        }

        return appliances;
    }


    // Method to return a bitmap from an images URL
    private Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {

            // See what we are getting
            Log.i(ContentValues.TAG, "" + url);

            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();

            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);

            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e(ContentValues.TAG, "Error getting bitmap", e);
        }
        return bm;
    }

}
