package com.uady.jorge_cano.t_search.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.uady.jorge_cano.t_search.R;

public class SearchActivity extends AppCompatActivity {

    private EditText searchField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchField = (EditText) findViewById(R.id.searchField);
        setupEnterListener();
        //searchField.setTypeface(Typeface.);
    }

    private void setupEnterListener() {
        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ( (actionId == EditorInfo.IME_ACTION_DONE) ||
                        ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) &&
                                (event.getAction() == KeyEvent.ACTION_DOWN ))){
                    searchClick(v);
                    return true;
                }
                else{
                    return false;
                }
            }
        });
    }

    public void searchClick(View view){

        if(!searchField.getText().toString().equals("")){

            Intent intent = new Intent(this, ResultsActivity.class);
            intent.putExtra("search_term", searchField.getText().toString());
            startActivity(intent);
        } else{

            Toast.makeText(
                    this.getApplicationContext(),
                    "No hay términos para buscar",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
}
