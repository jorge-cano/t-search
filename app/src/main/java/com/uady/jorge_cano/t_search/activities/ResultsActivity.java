package com.uady.jorge_cano.t_search.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.uady.jorge_cano.t_search.R;
import com.uady.jorge_cano.t_search.model.Appliance;
import com.uady.jorge_cano.t_search.model.comparators.BrandComparator;
import com.uady.jorge_cano.t_search.model.comparators.IntakeAscendingComparator;
import com.uady.jorge_cano.t_search.model.comparators.IntakeDescendingComparator;
import com.uady.jorge_cano.t_search.model.comparators.PriceAscendingComparator;
import com.uady.jorge_cano.t_search.model.comparators.PriceDescendingComparator;
import com.uady.jorge_cano.t_search.model.comparators.StoreComparator;
import com.uady.jorge_cano.t_search.model.SelectedAppliance;
import com.uady.jorge_cano.t_search.model.adapters.ApplianceListAdapter;
import com.uady.jorge_cano.t_search.tasks.AmazonAppliancesTask;
import com.uady.jorge_cano.t_search.tasks.ChedrauiAppliancesTask;
import com.uady.jorge_cano.t_search.tasks.LiverpoolAppliancesTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private List<Appliance> appliances;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("search_term"));
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        this.appliances = new ArrayList<>();

        //new ChedrauiAppliancesTask(getIntent().getStringExtra("search_term"), this).execute();
        new AmazonAppliancesTask(getIntent().getStringExtra("search_term"), this).execute();
        new LiverpoolAppliancesTask(getIntent().getStringExtra("search_term"), this).execute();

        setClickListener();
    }

    private void setClickListener() {

        ((ListView) findViewById(android.R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Appliance selectedAppliance = appliances.get(position);
                SelectedAppliance.getInstance().setAppliance(selectedAppliance);

                goToDetails();
            }
        });
    }

    private void goToDetails() {

        Intent intent = new Intent(this, ApplianceDetailsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.price_ascending) {

            Collections.sort(this.appliances, new PriceAscendingComparator());

        } else if (id == R.id.price_decending) {

            Collections.sort(this.appliances, new PriceDescendingComparator());

        } else if (id == R.id.intake_descending) {

            Collections.sort(this.appliances, new IntakeDescendingComparator());

        } else if (id == R.id.intake_ascending){

            Collections.sort(this.appliances, new IntakeAscendingComparator());

        }else if (id == R.id.store) {

            Collections.sort(this.appliances, new StoreComparator());

        } else if (id == R.id.brand) {

            Collections.sort(this.appliances, new BrandComparator());
        }

        this.displayApplianceList();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setApplianceSearchResults(ArrayList<Appliance> appliances) {
        this.appliances.addAll(appliances);
        Collections.sort(this.appliances, new PriceAscendingComparator());
        displayApplianceList();
    }

    private void displayApplianceList(){

        TextView loadingMessage = (TextView) findViewById(android.R.id.empty);
        if(loadingMessage.getVisibility() == View.VISIBLE){
            loadingMessage.setVisibility(View.INVISIBLE);
        }

        ApplianceListAdapter adapter = new ApplianceListAdapter(this, R.layout.appliance_list_item, this.appliances);
        ((ListView)findViewById(android.R.id.list)).setAdapter(adapter);
    }
}
