package com.uady.jorge_cano.t_search.model;

/**
 * Created by jorge-cano on 4/10/17.
 */

public class SelectedAppliance {

    private Appliance appliance;

    private static final SelectedAppliance ourInstance = new SelectedAppliance();

    public static SelectedAppliance getInstance() {
        return ourInstance;
    }

    private SelectedAppliance() {
    }

    public Appliance getAppliance() {
        return appliance;
    }

    public void setAppliance(Appliance appliance) {
        this.appliance = appliance;
    }
}
