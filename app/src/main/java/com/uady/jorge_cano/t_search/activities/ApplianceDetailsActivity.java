package com.uady.jorge_cano.t_search.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.uady.jorge_cano.t_search.R;
import com.uady.jorge_cano.t_search.model.Appliance;
import com.uady.jorge_cano.t_search.model.SelectedAppliance;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ApplianceDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appliance_details);

        setApplianceInfo();
    }

    private void setApplianceInfo() {

        Appliance appliance = SelectedAppliance.getInstance().getAppliance();

        ImageView image = (ImageView) findViewById(R.id.appliance_image);
        TextView priceLabel = (TextView) findViewById(R.id.appliance_price);
        TextView brandLabel = (TextView) findViewById(R.id.appliance_brand);
        TextView intakeLabel = (TextView) findViewById(R.id.appliance_intake);
        TextView assessmentLabel = (TextView) findViewById(R.id.appliance_assessment);
        TextView sizeLabel = (TextView) findViewById(R.id.appliance_size);
        TextView capacityLabel = (TextView) findViewById(R.id.appliance_capacity);
        TextView storeLabel = (TextView) findViewById(R.id.appliance_store);

        DecimalFormat decimalFormatter = new DecimalFormat();
        decimalFormatter.setMaximumFractionDigits(2);
        decimalFormatter.setMinimumFractionDigits(0);
        decimalFormatter.setGroupingUsed(false);


        setTitle(appliance.getModel());

        if(appliance.getImage() != null){
            image.setImageBitmap(appliance.getImage());
        } else {
            image.setImageBitmap(BitmapFactory.decodeResource(
                    getResources(),
                    R.drawable.img_not_available)
            );
        }

        String fullPrice;
        if(!appliance.getPrice().equals(new BigDecimal(0))){
            fullPrice = decimalFormatter.format(appliance.getPrice().setScale(2, BigDecimal.ROUND_DOWN))
                    + " " + appliance.getPriceCode();
        } else{
            fullPrice = "N/A";
        }

        priceLabel.setText(fullPrice);

        brandLabel.setText(appliance.getBrand());

        String fullIntake = "N/A";
        if(appliance.getIntake()!=0){
            fullIntake = appliance.getIntake() + "W";
        }
        intakeLabel.setText(fullIntake);

        assessmentLabel.setText(appliance.getAssessment());

        if(!appliance.getDimensions().equals("")) {
            sizeLabel.setText(appliance.getDimensions());
        } else{
            sizeLabel.setText("N/A");
        }

        if(!appliance.getCapacity().equals("")) {
            capacityLabel.setText(appliance.getCapacity());
        } else {
            capacityLabel.setText("N/A");
        }

        SpannableString content = new SpannableString(appliance.getStore());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        storeLabel.setText(content);
    }

    public void goToStore(View view){

        Intent browse = new Intent( Intent.ACTION_VIEW ,
                Uri.parse( SelectedAppliance.getInstance().getAppliance().getStoreURL() ) ); //poner la url del electrodoméstico
        startActivity( browse );
    }
}
