package com.uady.jorge_cano.t_search.tasks;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.uady.jorge_cano.t_search.activities.ResultsActivity;
import com.uady.jorge_cano.t_search.model.Appliance;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jorge-cano on 2/10/17.
 */

public class LiverpoolAppliancesTask extends AsyncTask<Void, Void, ArrayList<Appliance>> {

    private String searchUrl = "https://www.liverpool.com.mx/tienda/?s=";
    private String searchTerm;
    private String domain = "https://www.liverpool.com.mx";
    private ResultsActivity activity;

    public LiverpoolAppliancesTask(String searchTerm, ResultsActivity activity) {
        this.searchTerm = searchTerm.replace(" ", "+");
        this.activity = activity;
    }

    @Override
    protected ArrayList<Appliance> doInBackground(Void... params) {

        Document document = null;
        try {
            document = Jsoup.connect(this.searchUrl + this.searchTerm).get();
            Elements results = document.select("li.product-cell");

            ArrayList<Appliance> appliances = parseAppliances(results);

            return appliances;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return new ArrayList<>();
    }

    @Override
    protected void onPostExecute(ArrayList<Appliance> appliances) {
        this.activity.setApplianceSearchResults(appliances);
    }

    private ArrayList<Appliance> parseAppliances(Elements results) throws IOException{

        ArrayList<Appliance> appliances = new ArrayList<>();

        int counter = 0;

        try {
            for (Element result : results) {
                String productDetailsUrl = this.domain + result.select("a").first().attr("href");
                Document document = Jsoup.connect(productDetailsUrl)
                        .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0")
                        //.referrer("http://www.google.com")
                        .followRedirects(true)
                        .timeout(1200)
                        .header("Accept-Language", "en")
                        .header("Accept-Encoding", "gzip,deflate,sdch")
                        .header("Host", "www.liverpool.com.mx")
                        .get();

                String model = document.select("input[id=\"ga_prod_name\"]").attr("value");

                Bitmap image = getImageBitmap("https://ss625.liverpool.com.mx/xl/" +
                        document.select("input[id=\"ga_skuid\"]").attr("value") + ".jpg");

                BigDecimal price = new BigDecimal(document.select("input[id=\"ga_price\"]").attr("value"));
                String priceCode = "MXN";

                String brand = document.select("input[id=\"ga_brand\"]").attr("value");

                Elements intakeElements = document.select("span:contains(Potencia)").next();
                String stringIntake;
                if(intakeElements.size() > 1){
                    stringIntake = intakeElements.get(1).text().replaceAll("\\D+", "");
                } else{
                    stringIntake = intakeElements.text().replaceAll("\\D+", "");
                }

                int intake = 0;
                if (!stringIntake.equals("")) {
                    intake = Integer.valueOf(stringIntake);
                }

                String assessment = document.select("input[id=\"ajax_averageratings\"]").attr("value") + "/5";


                String dimensions = document.select("span:contains(Medidas)").next().text();
                if(dimensions.equals("")){
                    dimensions = document.select("span:contains(Dimensiones)").next().text();
                }

                String capacity = document.select("span:contains(Capacidad)").next().text();

                String store = "Liverpool";

                appliances.add(new Appliance(model, image, price, priceCode,
                        intake, assessment, dimensions, capacity, store, brand, productDetailsUrl));

                counter++;
                if(counter >= 20){
                    return appliances;
                }
            }
        } catch (SocketTimeoutException e){
            return appliances;
        }

        return appliances;
    }


    // Method to return a bitmap from an images URL
    private Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {

            // See what we are getting
            Log.i(ContentValues.TAG, "" + url);

            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();

            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);

            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e(ContentValues.TAG, "Error getting bitmap", e);
        }
        return bm;
    }

}
