package com.uady.jorge_cano.t_search.tasks;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.uady.jorge_cano.t_search.activities.ResultsActivity;
import com.uady.jorge_cano.t_search.model.Appliance;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jorge-cano on 2/10/17.
 */

public class AmazonAppliancesTask extends AsyncTask<Void, Void, ArrayList<Appliance>> {

    private String searchUrl = "https://www.amazon.com.mx/s/ref=nb_sb_noss?__mk_es_MX=ÅMÅŽÕÑ&field-keywords=";
    private String searchTerm;
    private String domain = "https://www.amazon.com.mx";
    private ResultsActivity activity;

    public AmazonAppliancesTask(String searchTerm, ResultsActivity activity) {
        this.searchTerm = searchTerm.replace(" ", "+");
        this.activity = activity;
    }

    @Override
    protected void onPostExecute(ArrayList<Appliance> appliances) {
        if(appliances.size() > 0) {
            this.activity.setApplianceSearchResults(appliances);
        }
    }

    @Override
    protected ArrayList<Appliance> doInBackground(Void... params) {

        Document document = null;
        try {
            document = Jsoup.connect(this.searchUrl + this.searchTerm).get();
            Elements results = document.select("li.s-result-item");

            ArrayList<Appliance> appliances = parseAppliances(results);

            return appliances;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return new ArrayList<Appliance>();
    }

    private ArrayList<Appliance> parseAppliances(Elements results) throws IOException{

        ArrayList<Appliance> appliances = new ArrayList<>();

        try {
            for (Element result : results) {
                String productDetailsUrl = result.select("a").first().attr("href");
                Document document = Jsoup.connect(productDetailsUrl)
                        .get();

                Element busyServer = document.select("title:contains(Server Busy)").first();
                if(busyServer != null){
                    continue;
                }

                String model = document.select("span[id=\"productTitle\"]").text();

                Bitmap image = getImageBitmap(document.select("img[id=\"landingImage\"]").attr("data-old-hires"));

                BigDecimal price = new BigDecimal(0);
                try {
                    String stringPrice = document.select("span[id=\"priceblock_ourprice\"]").text();
                    if (stringPrice != "") {

                        stringPrice = stringPrice.replace(",", "");
                        price = new BigDecimal(stringPrice.substring(1));
                    } else {

                        Element priceElement = document.select("span.a-color-price").first();
                        if(priceElement != null){
                            stringPrice = priceElement.text();
                            stringPrice = stringPrice.replace(",", "");
                            price = new BigDecimal(stringPrice.substring(1));
                        }

                    }
                }catch(NumberFormatException e){
                    price = new BigDecimal(0);
                }

                String priceCode = "MXN";

                String brand = document.select("td:contains(Marca)").next().text();

                int intake = 0;
                String wattage = document.select("td:contains(Vataje)").next().text();
                if(!wattage.equals("")){
                    intake = Integer.valueOf(wattage.replaceAll("\\D+", ""));
                }

                Element assessmentIcons = document.select("i.a-icon-star").select("span.a-icon-alt").first();
                String assessment = "N/A";
                if (assessmentIcons != null){
                    assessment = assessmentIcons.text()
                            .replace(" de un máximo de ", "/").replace(" estrellas", "");
                }

                String dimensions = "N/A";

                String capacity = "N/A";

                String store = "Amazon";

                appliances.add(new Appliance(model, image, price, priceCode,
                        intake, assessment, dimensions, capacity, store, brand, productDetailsUrl));

            }
        } catch (SocketTimeoutException e){
            return appliances;
        }

        return appliances;
    }

    // Method to return a bitmap from an images URL
    private Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {

            // See what we are getting
            Log.i(ContentValues.TAG, "" + url);

            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();

            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);

            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e(ContentValues.TAG, "Error getting bitmap", e);
        }
        return bm;
    }
}
