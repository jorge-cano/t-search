package com.uady.jorge_cano.t_search.model;

import android.graphics.Bitmap;

import java.math.BigDecimal;

/**
 * Created by jorge-cano on 1/10/17.
 */

public class Appliance {

    private String model;
    private Bitmap image;
    private BigDecimal price;
    private String brand;
    private int intake;
    private String assessment;
    private String dimensions;
    private String capacity;
    private String store;
    private String priceCode;
    private String storeURL;

    public Appliance(String model, Bitmap image, BigDecimal price, String priceCode, int intake, String assessment, String dimensions, String capacity, String store, String brand, String storeUrl) {
        this.model = model;
        this.image = image;
        this.price = price;
        this.intake = intake;
        this.assessment = assessment;
        this.dimensions = dimensions;
        this.capacity = capacity;
        this.store = store;
        this.brand = brand;
        this.priceCode = priceCode;
        this.storeURL = storeUrl;
    }

    public String getModel() {
        return model;
    }

    public Bitmap getImage() {
        return image;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getIntake() {
        return intake;
    }

    public String getAssessment() {
        return assessment;
    }

    public String getDimensions() {
        return dimensions;
    }

    public String getCapacity() {
        return capacity;
    }

    public String getStore() {
        return store;
    }

    public String getBrand() {
        return brand;
    }

    public String getPriceCode() {
        return priceCode;
    }

    public String getStoreURL() {
        return storeURL;
    }

    @Override
    public String toString() {
        return this.model;
    }
}
