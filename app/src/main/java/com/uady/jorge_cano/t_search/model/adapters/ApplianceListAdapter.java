package com.uady.jorge_cano.t_search.model.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uady.jorge_cano.t_search.R;
import com.uady.jorge_cano.t_search.model.Appliance;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by jorge-cano on 1/10/17.
 */

public class ApplianceListAdapter extends ArrayAdapter<Appliance> {

    private final Bitmap defaultImage = BitmapFactory.decodeResource(
            getContext().getResources(),
            R.drawable.img_not_available);

    public ApplianceListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ApplianceListAdapter(Context context, int resource, List<Appliance> items) {
        super(context, resource, items);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View applianceListView = convertView;

        if (applianceListView == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            applianceListView = vi.inflate(R.layout.appliance_list_item, null);
        }

        DecimalFormat decimalFormatter = new DecimalFormat();
        decimalFormatter.setMaximumFractionDigits(2);
        decimalFormatter.setMinimumFractionDigits(0);
        decimalFormatter.setGroupingUsed(false);

        Appliance appliance = getItem(position);

        ImageView applianceImage = (ImageView) applianceListView.findViewById(R.id.list_icon);
        if(appliance.getImage() != null) {
            applianceImage.setImageBitmap(appliance.getImage());
        } else {
            applianceImage.setImageBitmap(this.defaultImage);
        }

        ((TextView) applianceListView.findViewById(R.id.list_model)).setText(appliance.getModel());

        String price;
        if(!appliance.getPrice().equals(new BigDecimal(0))){
             price = decimalFormatter.format(appliance.getPrice().setScale(2, BigDecimal.ROUND_DOWN))
                    + " " +  appliance.getPriceCode();
        } else{
            price = "N/A";
        }

        ((TextView) applianceListView.findViewById(R.id.list_price)).setText(price);

        ((TextView) applianceListView.findViewById(R.id.list_brand)).setText(appliance.getBrand());

        if(appliance.getIntake() > 0){
            String formattedIntake = String.valueOf(appliance.getIntake()) + "W";
            ((TextView) applianceListView.findViewById(R.id.list_intake)).setText(formattedIntake);
        } else{
            ((TextView) applianceListView.findViewById(R.id.list_intake)).setText("N/A");
        }

        ((TextView) applianceListView.findViewById(R.id.list_store)).setText(appliance.getStore());

        return applianceListView;

    };
}
