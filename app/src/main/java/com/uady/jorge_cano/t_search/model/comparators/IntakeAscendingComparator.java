package com.uady.jorge_cano.t_search.model.comparators;

import com.uady.jorge_cano.t_search.model.Appliance;

import java.util.Comparator;

/**
 * Created by jorge-cano on 1/10/17.
 */

public class IntakeAscendingComparator implements Comparator<Appliance>{

    @Override
    public int compare(Appliance o1, Appliance o2) {

        return o1.getIntake() - o2.getIntake();
    }
}
